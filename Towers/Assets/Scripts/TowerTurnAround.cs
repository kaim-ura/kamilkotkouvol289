﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTurnAround : MonoBehaviour
{
    private void Start()
    {
        InvokeRepeating(nameof(TurnAround), 2.1f, 2.1f);
    }

    private void TurnAround()
    {
        transform.Rotate(0f, 0f, 15f);
    }
}
