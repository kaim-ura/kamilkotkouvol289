﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShoot : MonoBehaviour
{
    public Rigidbody2D projectile;
    public float speed = 4;

    private void Start()
    {
        InvokeRepeating(nameof(Shoot), 2f, 2f);
    }
    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        Rigidbody2D bullet = Instantiate(projectile, transform.position, transform.rotation);
        bullet.velocity = transform.up * speed;
    }

    public void DestroyProjectile()
    {
        Destroy(projectile);
    }
}
