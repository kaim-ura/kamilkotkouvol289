﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControll : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoordinates;

    private void OnMouseDown()
    {
        mZCoordinates = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        // Store offset = gameobject world position - mouse world position
        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        // pixel coordinates (x, y)
        Vector3 mousePoint = Input.mousePosition;

        // z coordinates of gameobject on screen
        mousePoint.z = mZCoordinates;

        //Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
    }
}
